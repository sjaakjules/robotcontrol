  

#define SERVO_1_PULSE_LEFT 800
#define SERVO_1_PULSE_RIGHT 1500
#define SERVO_1_HOME_PULSE 1290
#define SERVO_1_MAX_PULSE 1600
#define SERVO_1_MIN_PULSE 400
#define SERVO_1_MAX_ANGLE 90
#define SERVO_1_MIN_ANGLE -90




#define SERVO_2_PULSE_LEFT 700
#define SERVO_2_PULSE_RIGHT 1390
#define SERVO_2_HOME_PULSE 1130
#define SERVO_2_MAX_PULSE 1570
#define SERVO_2_MIN_PULSE 500
#define SERVO_2_MAX_ANGLE 80
#define SERVO_2_MIN_ANGLE -75


#define SERVO_3_PULSE_LEFT 550
#define SERVO_3_PULSE_RIGHT 1200
#define SERVO_3_HOME_PULSE 1100
#define SERVO_3_MAX_PULSE 1300
#define SERVO_3_MIN_PULSE 200
#define SERVO_3_MAX_ANGLE 100
#define SERVO_3_MIN_ANGLE -65


#define SERVO_4_PULSE_LEFT 550
#define SERVO_4_PULSE_RIGHT 1200
#define SERVO_4_HOME_PULSE 740
#define SERVO_4_MAX_PULSE 1300
#define SERVO_4_MIN_PULSE 150
#define SERVO_4_MAX_ANGLE 30
#define SERVO_4_MIN_ANGLE -100


#define SERVO_5_PULSE_LEFT 560
#define SERVO_5_PULSE_RIGHT 1800
#define SERVO_5_HOME_PULSE 925
#define SERVO_5_MAX_PULSE 1750
#define SERVO_5_MIN_PULSE 160
#define SERVO_5_MAX_ANGLE 60
#define SERVO_5_MIN_ANGLE -60


#define SERVO_6_PULSE_LEFT 600
#define SERVO_6_PULSE_RIGHT 1800
#define SERVO_6_HOME_PULSE 1230
#define SERVO_6_MAX_PULSE 2350
#define SERVO_6_MIN_PULSE 300
#define SERVO_6_MAX_ANGLE 110
#define SERVO_6_MIN_ANGLE -70


#define GRIPPER_OPEN_PULSE 1500
#define GRIPPER_CLOSE_PULSE 800


#define TRAJSTEPNUM 50   //Size of the trajectory in terms of the number of steps


#include <Servo.h>

Servo servo1;
Servo servo2;
Servo servo3;
Servo servo4;
Servo servo5;
Servo servo6;
Servo servo7;


int angle;
int feedbackPin1 = A0;  //feedback analog read pin
int feedbackPin2 = A1;
int feedbackPin3 = A2;
int feedbackPin4 = A3;
int feedbackPin5 = A8;
int feedbackPin6 = A9;
int feedbackPin7 = A11;


int oldpulse;  //previous, or 'old' servo x pulse lenght
int newpulse;  //current, or 'new' servo x pulse lenght
int motionloopstart; //this is the counter for the motion control
int reportloopstart; //this is the counter for reporting back to processing
int trajectory1[TRAJSTEPNUM+1]; //create more for other servos
int trajectory2[TRAJSTEPNUM+1];
int trajectory3[TRAJSTEPNUM+1];
int trajectory4[TRAJSTEPNUM+1];
int trajectory5[TRAJSTEPNUM+1];
int trajectory6[TRAJSTEPNUM+1];

int stepsize;
int index_1=51; //create more for other servos. The value is bigger than TRAJSTEPNUM to avoid triggering the movement loop before any command
int index_2=51; // in other words, we want to be able to initialize the robot without any movement.
int index_3=51;
int index_4=51;
int index_5=51;
int index_6=51;

int servo1_fb_left;
int servo1_fb_right;
int servo2_fb_left;
int servo2_fb_right;
int servo3_fb_left;
int servo3_fb_right;
int servo4_fb_left;
int servo4_fb_right;
int servo5_fb_left;
int servo5_fb_right;
int servo6_fb_left;
int servo6_fb_right;





float m1_p2a[] = {2.3308397354273e-12, -1.1888643189475e-08, 2.3324806288259e-05, -2.2051916153756e-02, 9.9396171898246e+00, -1.6058959260380e+03};
float m2_p2a[] = {-1.5807808761132e-12, 8.7816482447290e-09, -1.9018676932884e-05, 1.9928997487491e-02, -1.0131182107996e+01, 2.0369039491148e+03};
float m3_p2a[] = {-2.9157603087293e-12, 1.0933419800629e-08, -1.5669094867987e-05, 1.0518102347084e-02, -3.3262888487901e+00, 4.7389118424948e+02};
float m4_p2a[] = {-1.2888161249439e-12, 5.5040391715872e-09, -8.8373102680828e-06, 6.3929227562843e-03, -2.0561975544101e+00, 2.3942698087029e+02};
float m5_p2a[] = {-6.8184968027163e-14, 4.0353332719020e-10, -8.9853101193045e-07, 9.2102002080995e-04, -4.9480876703033e-01, 1.3467407215432e+02};
float m6_p2a[] = {-1.7293637792045e-14, 1.6028190711457e-10, -5.5853459420064e-07, 8.9557083242935e-04, -7.0337149750859e-01, 2.3216564379025e+02};

float m1_a2p[] = {1.1035265067225e-08, 2.6966326271464e-07, -4.7677894259133e-04, -3.2298132033230e-02, -3.1184079602675e+00, 1.2779721415591e+03};
float m2_a2p[] = {-1.7886920389795e-08, 2.3375480774302e-06, 6.0933414435034e-04, -2.6490771391792e-02, -1.0218860332279e+01, 1.0968675133510e+03};
float m3_a2p[] = {4.6355524536277e-09, 1.5156540149543e-06, -9.3126217382782e-05, -4.3237369299912e-02, -4.5904329286566e+00, 1.0915965598142e+03};
float m4_a2p[] = {1.3234154133347e-07, 2.9078503266961e-05, 1.0350102566844e-03, -1.2813153527575e-01, -1.3087225695986e+01, 6.8686249266844e+02};
float m5_a2p[] = {-4.4681490384614e-09, 8.0747377622378e-07, 5.1705692744755e-04, 1.0843793706294e-02, -1.4599513461538e+01, 9.2532433566434e+02};
float m6_a2p[] = {-3.4387560389652e-08, 2.1372675649661e-06, 8.5959040873467e-04, 6.2644561118020e-03, -1.7780071370863e+01, 1.2131047825923e+03};



int stepinterval = 100; //amount of milliseconds over which the position will be updated. If too small, the changes might be too quick for the physical servo to follow.

void setup(){
Serial.begin(19200); // set the baud rate
Serial.println("Ready"); // print "Ready" once


motionloopstart = millis();
reportloopstart = millis();

delay(1000);

delay(200);
servo1.attach(9);
servo1.writeMicroseconds(SERVO_1_PULSE_LEFT);
delay(500);
delay(200);
delay(200);

servo1_fb_left = analogRead(feedbackPin1);
delay(200);
servo1.writeMicroseconds(SERVO_1_PULSE_RIGHT);
delay(500);
delay(200);
delay(200);
servo1_fb_right = analogRead(feedbackPin1);
delay(200);
//servo1.writeMicroseconds(SERVO_1_HOME_PULSE);
delay(200);

servo1.detach();


delay(200);
servo2.attach(8);
servo2.writeMicroseconds(SERVO_2_PULSE_LEFT);
delay(700);
servo2_fb_left = analogRead(feedbackPin2);
delay(200);
servo2.writeMicroseconds(SERVO_2_PULSE_RIGHT);
delay(700);
servo2_fb_right = analogRead(feedbackPin2);
delay(200);
//servo2.writeMicroseconds(SERVO_2_HOME_PULSE);
delay(200);

servo2.detach();


delay(200);
servo3.attach(7);
servo3.writeMicroseconds(SERVO_3_PULSE_LEFT);
delay(700);
servo3_fb_left = analogRead(feedbackPin3);
delay(200);
servo3.writeMicroseconds(SERVO_3_PULSE_RIGHT);
delay(700);
servo3_fb_right = analogRead(feedbackPin3);
delay(200);
//servo3.writeMicroseconds(SERVO_3_HOME_PULSE);
delay(200);

servo3.detach();


delay(200);
servo4.attach(6);
servo4.writeMicroseconds(SERVO_4_PULSE_LEFT);
delay(700);
servo4_fb_left = analogRead(feedbackPin4);
delay(200);
servo4.writeMicroseconds(SERVO_4_PULSE_RIGHT);
delay(700);
servo4_fb_right = analogRead(feedbackPin4);
delay(200);
//servo4.writeMicroseconds(SERVO_4_HOME_PULSE);
delay(200);

servo4.detach();


delay(200);
servo5.attach(5);
servo5.writeMicroseconds(SERVO_5_PULSE_LEFT);
delay(700);
servo5_fb_left = analogRead(feedbackPin5);
delay(200);
servo5.writeMicroseconds(SERVO_5_PULSE_RIGHT);
delay(700);
servo5_fb_right = analogRead(feedbackPin5);
delay(200);
//servo5.writeMicroseconds(SERVO_5_HOME_PULSE);
delay(200);

servo5.detach();


delay(200);
servo6.attach(4);
servo6.writeMicroseconds(SERVO_6_PULSE_LEFT);
delay(700);
servo6_fb_left = analogRead(feedbackPin6);
delay(200);
servo6.writeMicroseconds(SERVO_6_PULSE_RIGHT);
delay(700);
servo6_fb_right = analogRead(feedbackPin6);
delay(200);
//servo6.writeMicroseconds(SERVO_6_HOME_PULSE);
delay(200);

servo6.detach();

homeposition();


}

void loop() {
  if(Serial.available())
  { // only send data back if data has been sent
    char inByte = Serial.read(); // read the incoming data

      switch (inByte) 
      {
      case 'a':
        angle = Serial.parseInt();
       newpulse= map_poly(angle,m1_a2p[0],m1_a2p[1],m1_a2p[2],m1_a2p[3],m1_a2p[4],m1_a2p[5], SERVO_1_MAX_PULSE, SERVO_1_MIN_PULSE) ;                           //experimental: converts pot to pwm
               trajectory1[1]=(newpulse);
        servo1.attach(9);
        break;
            
       case 'b':
        angle = Serial.parseInt();
       newpulse= map_poly(angle,m2_a2p[0],m2_a2p[1],m2_a2p[2],m2_a2p[3],m2_a2p[4],m2_a2p[5], SERVO_2_MAX_PULSE, SERVO_2_MIN_PULSE) ;  //        oldpulse = map(analogRead(feedbackPin2),155,482,500,1650);
               trajectory2[1]=(newpulse);
                servo2.attach(8);
        
        break;   
      
       case 'c':
        angle = Serial.parseInt();
       newpulse= map_poly(angle,m3_a2p[0],m3_a2p[1],m3_a2p[2],m3_a2p[3],m3_a2p[4],m3_a2p[5], SERVO_3_MAX_PULSE, SERVO_3_MIN_PULSE) ;         // oldpulse = map(analogRead(feedbackPin3),155,482,500,1650);  
               trajectory3[1]=(newpulse);
               servo3.attach(7);
        
        break; 
        
             case 'd':
        angle = Serial.parseInt();
       newpulse= map_poly(angle,m4_a2p[0],m4_a2p[1],m4_a2p[2],m4_a2p[3],m4_a2p[4],m4_a2p[5], SERVO_4_MAX_PULSE, SERVO_4_MIN_PULSE) ;          //oldpulse = map(analogRead(feedbackPin4), 155,482,500,1650);
               trajectory4[1]=(newpulse);
        servo4.attach(6);
        break;  
            
         case 'e':
        angle = Serial.parseInt();
       newpulse= map_poly(angle,m5_a2p[0],m5_a2p[1],m5_a2p[2],m5_a2p[3],m5_a2p[4],m5_a2p[5], SERVO_5_MAX_PULSE, SERVO_5_MIN_PULSE) ;        
               trajectory5[1]=(newpulse);
        servo5.attach(5);
        break;
        
        
         case 'f':
        angle = Serial.parseInt();
       newpulse= map_poly(angle,m6_a2p[0],m6_a2p[1],m6_a2p[2],m6_a2p[3],m6_a2p[4],m6_a2p[5] , SERVO_6_MAX_PULSE, SERVO_6_MIN_PULSE) ;        
               trajectory6[1]=(newpulse);
        servo6.attach(4);
        break;
        
      case 'r':   //run homeposition function
        relax();
        break;
            
      case 'h':   //run homeposition function
        homeposition();
        break;
        
      case 'o':  //open gripper
        gripperopen();
        break;
        
      case 'g':   //close gripper
       gripperclose();
       break;
      
      }
  }
  
  if(millis()-reportloopstart > 750){   //every second, report position of servo
    Serial.println("A"+String(analogRead(feedbackPin1))); 
    Serial.println("B"+String(analogRead(feedbackPin2)));
    Serial.println("C"+String(analogRead(feedbackPin3))); 
    Serial.println("D"+String(analogRead(feedbackPin4))); 
    Serial.println("E"+String(analogRead(feedbackPin5))); 
    Serial.println("F"+String(analogRead(feedbackPin6))); 

    //reportloopstart = millis();
  }  
  
  
  


  if(millis()-reportloopstart > 1000){   //every second, report position of servo
    Serial.println("a"+String(map_f2a(analogRead(feedbackPin1),servo1_fb_left,servo1_fb_right,SERVO_1_PULSE_LEFT,SERVO_1_PULSE_RIGHT, m1_p2a, SERVO_1_MAX_ANGLE, SERVO_1_MIN_ANGLE)));         // corrected
    Serial.println("b"+String(map_f2a(analogRead(feedbackPin2),servo2_fb_left,servo2_fb_right,SERVO_2_PULSE_LEFT,SERVO_2_PULSE_RIGHT, m2_p2a, SERVO_2_MAX_ANGLE, SERVO_2_MIN_ANGLE)));          // corrected
    Serial.println("c"+String(map_f2a(analogRead(feedbackPin3),servo3_fb_left,servo3_fb_right,SERVO_3_PULSE_LEFT,SERVO_3_PULSE_RIGHT, m3_p2a, SERVO_3_MAX_ANGLE, SERVO_3_MIN_ANGLE)));   
    Serial.println("d"+String(map_f2a(analogRead(feedbackPin4),servo4_fb_left,servo4_fb_right,SERVO_4_PULSE_LEFT,SERVO_4_PULSE_RIGHT, m4_p2a, SERVO_4_MAX_ANGLE, SERVO_4_MIN_ANGLE)));    // send the data back in a new line so that it is not all one long line
    Serial.println("e"+String(map_f2a(analogRead(feedbackPin5),servo5_fb_left,servo5_fb_right,SERVO_5_PULSE_LEFT,SERVO_5_PULSE_RIGHT, m5_p2a, SERVO_5_MAX_ANGLE, SERVO_5_MIN_ANGLE)));    // send the data back in a new line so that it is not all one long line
    Serial.println("f"+String(map_f2a(analogRead(feedbackPin6),servo6_fb_left,servo6_fb_right,SERVO_6_PULSE_LEFT,SERVO_6_PULSE_RIGHT, m6_p2a, SERVO_6_MAX_ANGLE, SERVO_6_MIN_ANGLE)));   // send the data back in a new line so that it is not all one long line

    reportloopstart = millis();
  }

  if(millis()-motionloopstart > stepinterval){   //every second, report position of servo
     if(trajectory1[0] != trajectory1[1]){
         servo1.writeMicroseconds(trajectory1[1]); 
         trajectory1[0] = trajectory1[1];
     }
     if(trajectory2[0] != trajectory2[1]){
         servo2.writeMicroseconds(trajectory2[1]); 
         trajectory2[0] = trajectory2[1];     
     }
     if(trajectory3[0] != trajectory3[1]){
         servo3.writeMicroseconds(trajectory3[1]); 
         trajectory3[0] = trajectory3[1];     
     }
     if(trajectory4[0] != trajectory4[1]){
         servo4.writeMicroseconds(trajectory4[1]); 
         trajectory4[0] = trajectory4[1];     
     }     
     if(trajectory5[0] != trajectory5[1]){
         servo5.writeMicroseconds(trajectory5[1]); 
         trajectory5[0] = trajectory5[1];     
     }       
     if(trajectory6[0] != trajectory6[1]){
         servo6.writeMicroseconds(trajectory6[1]); 
         trajectory6[0] = trajectory6[1];     
     }        
     motionloopstart = millis();
  }

}

void relax()
{
  servo1.detach();
  servo2.detach();
  servo3.detach();
  servo4.detach();
  servo5.detach();
  servo6.detach();
  
  servo7.detach();   //relax gripper

  Serial.println("Relaxing Robot");
}

void homeposition()
{

  servo1.attach(9);
  servo2.attach(8);
  servo3.attach(7);
  servo4.attach(6);
  servo5.attach(5);
  servo6.attach(4);

  
  servo1.writeMicroseconds(SERVO_1_HOME_PULSE);
  servo2.writeMicroseconds(SERVO_2_HOME_PULSE);
  servo3.writeMicroseconds(SERVO_3_HOME_PULSE);
  servo4.writeMicroseconds(SERVO_4_HOME_PULSE);
  servo5.writeMicroseconds(SERVO_5_HOME_PULSE);
  servo6.writeMicroseconds(SERVO_6_HOME_PULSE);
  
    Serial.println("Home Position");

}


int map_poly(int desired,float p1,float p2,float p3,float p4,float p5,float p6, float max_val, float min_val){
  
  float result = p1*pow(desired,5)+p2*pow(desired,4)+p3*pow(desired,3)+p4*pow(desired,2)+p5*desired+p6;
   
   if(result > max_val){
    return max_val; 
   }
   if(result < min_val){
    return min_val; 
   }
   
   
  return result;
}



int map_f2a(int fb_in, int fb_left, int fb_right, int pulse_left, int pulse_right, float p2a_array[], float max_val, float min_val){
 
 int desired_pulse = map(fb_in, fb_left, fb_right, pulse_left, pulse_right);

  int result_angle =  map_poly(desired_pulse, p2a_array[0],p2a_array[1],p2a_array[2],p2a_array[3],p2a_array[4],p2a_array[5],max_val,min_val); 
    return result_angle;
}

void gripperopen(){
 
  servo7.attach(2);
  
  servo7.writeMicroseconds(GRIPPER_OPEN_PULSE);
    delay(500);

  servo7.detach();
      Serial.println("Gripper Open");

}

void gripperclose(){
  
  servo7.attach(2);
  
  servo7.writeMicroseconds(GRIPPER_CLOSE_PULSE);
  
        Serial.println("Gripper Close");

}

