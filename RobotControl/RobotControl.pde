import remixlab.proscene.*;
import papaya.*;
import controlP5.*;
import processing.serial.*;
import org.gwoptics.graphics.graph2D.Graph2D;
import org.gwoptics.graphics.graph2D.LabelPos;
import org.gwoptics.graphics.graph2D.traces.Line2DTrace;
import org.gwoptics.graphics.graph2D.traces.ILine2DEquation;
import java.util.*;

System system;
GUI gui;
UI ui;
Model model;
Robot robot;
Master_Program mp;
//Interpret_Command ic;

void setup() {
  system = new System();
  gui = new GUI();  
  ui = new UI(this);
  robot = new Robot();
  model = new Model();
  mp = new Master_Program();
  //ic = new Interpret_Command();
    ardusetup();

}

void draw() {
  system.update();
  serialEvent(myPort);
  robot.update();
  model.draw();
  mp.draw();
  gui.draw();
    ardudraw();
    //ic.initial_run(ic.sample);

  //print("\n Angles: ");
  //Mat.print(robot.motorAngles(),2);
  //sendmotors();

  
}


