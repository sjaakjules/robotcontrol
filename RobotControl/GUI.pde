/* This class is used to handle all GUI functionality. It is shown above the global content for ease of access.
  */
class GUI {
  // Fields 
  private PFont _title, _display;
  
  // Constructors
  GUI () {
    size(1280,720,P2D);                                      // Size of the whole aplication
    frameRate(60);                                           // Framerate but cant seem to be faster tha int startTime;
    _title = loadFont("PresaANTIPIXEL.COM.AR-48.vlw");      // Various fonts used to display (tools>create font to add your own)
    _display = loadFont("SegoeUIEmoji-20.vlw");  
  }
  
  // Properties
  PFont display() {return _display;}
  PFont title() {return _title;}

  // Methods
  void draw() {  
    background(#93D1E0);
    textFont(_display, 15);
    text(int(frameRate),1250,20);
    textFont(_title,48);
    fill(#036AA0);
    textAlign(CENTER, TOP);
    text("Robotics  Test  Environment", 640,10);
    robot.H0te().printLocation(200,100);
    robot.printMotorAngles(140,180);
    robot.J().printJoc(700, 100);
    image(model.getImg(),0,360);
  }

  
}

/*  The following is all the ControlP5 objects, sliders, knobs and buttons
    where they are all global variables to allow all classes access. 
    -- The setup is called in the constructor of GUI --
    */
    
class UI {
  // Fields
  ControlP5 cp5;
  Textfield time;
  Textfield[] gain;
  Slider pX1, pY1, pZ1, rX1,rY1,rZ1;
  Button go, home,point1,point2,relax,GClose,GOpen;
  PImage[] Point1_imgs,Point2_imgs,Move_imgs,Home_imgs, saved;
  
  // Contructor
  UI(PApplet program) {
    smooth();
    noStroke();
    cp5 = new ControlP5(program);      
    saved =new PImage[] {loadImage("point1b.png"),loadImage("point2b.png")};
    Point1_imgs = new PImage[] {loadImage("point1a.png"),loadImage("point1d.png"),loadImage("point1c.png")};
    Point2_imgs = new PImage[] {loadImage("point2a.png"),loadImage("point2d.png"),loadImage("point2c.png")};
    Home_imgs = new PImage[] {loadImage("home1.png"),loadImage("home2.png"),loadImage("home3.png")};
    Move_imgs = new PImage[] {loadImage("move1.png"),loadImage("move2.png"),loadImage("move3.png")};
    gain = new Textfield[6];
    
    gain[0] = cp5.addTextfield("Gain_1")
                   .setPosition(20,100)
                   .setSize(100,25)
                   .setFont(gui.display())
                   .setColor(color(#004E74))
                   .setAutoClear(false)
                   .setColorBackground(color(#004E74,30))
                   .setText("0")
                   ;
    gain[1] = cp5.addTextfield("Gain_2")
                   .setPosition(20,140)
                   .setSize(100,25)
                   .setFont(gui.display())
                   .setColor(color(#004E74))
                   .setAutoClear(false)
                   .setColorBackground(color(#004E74,30))
                   .setText("0")
                   ;
    gain[2] = cp5.addTextfield("Gain_3")
                   .setPosition(20,180)
                   .setSize(100,25)
                   .setFont(gui.display())
                   .setColor(color(#004E74))
                   .setAutoClear(false)
                   .setColorBackground(color(#004E74,30))
                   .setText("0")
                   ;
    gain[3] = cp5.addTextfield("Gain_4")
                   .setPosition( 20,220)
                   .setSize(100,25)
                   .setFont(gui.display())
                   .setColor(color(#004E74))
                   .setAutoClear(false)
                   .setColorBackground(color(#004E74,30))
                   .setText("0")
                   ;
    gain[4] = cp5.addTextfield("Gain_5")
                   .setPosition( 20,260)
                   .setSize(100,25)
                   .setFont(gui.display())
                   .setColor(color(#004E74))
                   .setAutoClear(false)
                   .setColorBackground(color(#004E74,30))
                   .setText("0")
                   ;
    gain[5] = cp5.addTextfield("Gain_6")
                   .setPosition( 20,300)
                   .setSize(100,25)
                   .setFont(gui.display())
                   .setColor(color(#004E74))
                   .setAutoClear(false)
                   .setColorBackground(color(#004E74,30))
                   .setText("0")
                   ;
    pX1 =  cp5.addSlider("Start X position")
             .setPosition(300,100)
             .setRange(0,210)
             .setValue(40) 
             ;
    pY1 =  cp5.addSlider("Start Y position")
             .setPosition(300,120)
             .setRange(-150,150)
             .setValue(-130)            
             ;
    pZ1 =  cp5.addSlider("Start Z position")
             .setPosition(300,140)
             .setRange(0,210)
             .setValue(165)  
             ;
    rX1 =  cp5.addSlider("Start X rotation")
             .setPosition(300,160)
             .setRange(-90,90)
             .setValue(0) 
             ;
    rY1 =  cp5.addSlider("Start Y rotation")
             .setPosition(300,180)
             .setRange(-90,90)
             .setValue(0) 
             ;
    rZ1 =  cp5.addSlider("Start Z rotation")
             .setPosition(300,200)
             .setRange(-90,90)
             .setValue(0) 
             ;
             
    time = cp5.addTextfield("Time (sec)")
              .setPosition(500,100)
              .setSize(100,25)
              .setFont(gui.display())
              .setColor(color(#004E74))
              .setAutoClear(false)
              .setColorBackground(color(#004E74,30))
              .setText(str(1)) 
              ;
    go = cp5.addButton("go")
       .setValue(0)
       .setPosition(500,150)
       .setImages(Move_imgs)
       .updateSize()
       ;
       
    home = cp5.addButton("Home")
       .setValue(0)
       .setPosition(560,150)
       .setImages(Home_imgs)
       .updateSize()
       ;
       
    point1 = cp5.addButton("point1")
       .setValue(0)
       .setPosition(500,210)
       .setImages(Point1_imgs)
       .updateSize()
       ;
    point2 = cp5.addButton("point2")
       .setValue(0)
       .setPosition(560,210)
       .setImages(Point2_imgs)
       .updateSize()
       ;
       
    relax = cp5.addButton("Relax")
       .setPosition(450,350)
       .setSize(200,50)
       .setValue(0)
       ;
       
    GOpen = cp5.addButton("GOpen")
       .setValue(100)
       .setPosition(450,420)
       .setSize(80,50)
       ;
  
   GClose = cp5.addButton("GClose")
       .setValue(100)
       .setPosition(550,420)
       .setSize(80,50)
       ;
  }
  
  // Properties
  
  // Methods
  void controlSetup() {
    
  }
  
  void Gain_1(String theValue) {
    robot.posControl()._gain[0] = float(theValue);
    
  }
  
  void freezeControls() {
    pX1.lock() ;
    pY1.lock() ;
    pZ1.lock() ;
    rX1.lock() ;
    rY1.lock() ;
    rZ1.lock() ;
    time.lock();
    go.lock() ;
  }
  void unfreezeControls() {
    pX1.unlock() ;
    pY1.unlock() ;
    pZ1.unlock() ;
    rX1.unlock() ;
    rY1.unlock() ;
    rZ1.unlock() ;
    time.unlock();
    go.unlock() ;
  }
  
  boolean savePoint(int point) {
    if (point==1) {
      Point1_imgs[0] = saved[0];
      return true;
    } else if (point==2) {
      Point2_imgs[0] = saved[1];
      return true;
    }
    return false;
  }
      
}


