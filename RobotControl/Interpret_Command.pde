class Interpret_Command {
  
  
  //String[] sample;
  String[] interpret_output;
  int output_size_counter;
  int output_pos_counter;
  String[] sample = {"H","P 200 200 200","L 50 200 0 50 150 0","O","C","S 0 0 0"};
  
  
  String[] initial_run(String[] interpret_input){    
    output_size_counter = 0;
    
  for (int i = 0; i < interpret_input.length; i++){
    switch(interpret_input[i].charAt(0)){     
      case 'H': 
            output_size_counter = output_size_counter+1;
        break;
      
      case 'P':
            output_size_counter = output_size_counter+1;
        break;
      
      case 'L':
            output_size_counter = output_size_counter+2;
        break;
      
      case 'O':
            output_size_counter = output_size_counter+1;
        break;
      
      case 'C':
            output_size_counter = output_size_counter+1;
        break;
      
      case 'S':
            output_size_counter = output_size_counter+3;
        break;
    }
  }
  
  String interpret_output[] = new String[output_size_counter];
  output_pos_counter = 0;
  
  for (int j = 0; j < interpret_input.length; j++){  
    switch(interpret_input[j].charAt(0)){
      case 'H': 
          interpret_output[output_pos_counter] = "H|"+"t";
                println(interpret_output[output_pos_counter]);
          output_pos_counter = output_pos_counter+1;
        break;
      case 'P':
          String[] split_list_p = split(interpret_input[j], " ");
          interpret_output[output_pos_counter] = "M|"+ split_list_p[1]+","+split_list_p[2]+","+split_list_p[3]+"|t";
                println(interpret_output[output_pos_counter]);
          output_pos_counter = output_pos_counter+1;
        break;
      case 'L':
          String[] split_list_l = split(interpret_input[j], " ");
          interpret_output[output_pos_counter] = "M|"+ split_list_l[1]+","+split_list_l[2]+","+split_list_l[3]+"|t";
          interpret_output[output_pos_counter+1] = "M|"+split_list_l[4]+","+split_list_l[5]+","+split_list_l[6]+"|t";
                println(interpret_output[output_pos_counter]);
                println(interpret_output[output_pos_counter+1]);
          output_pos_counter = output_pos_counter+2;
        break;
      case 'O':
        interpret_output[output_pos_counter] = "O|"+"t";
                println(interpret_output[output_pos_counter]);
        output_pos_counter = output_pos_counter+1;
        break; 
      case 'C':
          interpret_output[output_pos_counter] = "C|"+"t";
                        println(interpret_output[output_pos_counter]);
          output_pos_counter = output_pos_counter+1;
        break;
      case 'S':
          String[] split_list_s = split(interpret_input[j], " ");
          int z_pos_s = int(split_list_s[3])+100;
          interpret_output[output_pos_counter] = "M|"+round(robot.endPosition()[0]+0.5)+","+round(robot.endPosition()[1]+0.5)+","+z_pos_s+"|"+split_list_s[1]+","+split_list_s[2]+","+z_pos_s+"|t";
          interpret_output[output_pos_counter+1] = "M|"+split_list_s[1]+","+split_list_s[2]+","+split_list_s[3]+"|t";
          interpret_output[output_pos_counter+2] = "C|"+"t";
                       println(interpret_output[output_pos_counter]);
                       println(interpret_output[output_pos_counter+1]);
                       println(interpret_output[output_pos_counter+2]);
          output_pos_counter = output_pos_counter+3;
        break;    
        }
      }
    return interpret_output;
   }
}
