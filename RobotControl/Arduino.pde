// Read angle values robot.motorAngles()   it is a float[]
// Write angle values robot.motorsupdateAngles(float angle) 

// The serial port:
Serial myPort;       
String dataReading;
// The angle reading strings
String[] pot;
String gpmsg;


String title = "Welcome. To control robot, input the desired angles at each joint";
String angle_1 = "a";
String angle_2 = "a";
String angle_3 = "a";
String angle_4 = "a";
String angle_5 = "a";
String angle_6 = "a";

String pot_1 = ".";
String pot_2 = ".";
String pot_3 = ".";
String pot_4 = ".";
String pot_5 = ".";
String pot_6 = ".";

 
void ardusetup(){
  // Initilise angle reading strings
  if(robot._isPluggedIn) {
    
    pot = new String[6];
    
    for (int i=0;i<6;i++) {
      pot[i] = " ";
    }
  
    dataReading = " "; 
    gpmsg = "waiting";
  
    
    // List all the available serial ports:
    
   
    println(Serial.list());
    
      myPort = new Serial(this, Serial.list()[2], 19200);
      myPort.bufferUntil('\n');
    
  }
  
}

void ardudraw(){
   textSize(14);
  text(title, 25, 25);
  textSize(20);
  fill(50);

  text("Angle", 870, 345);
  text(angle_1, 870, 365);
  text(angle_2, 870, 400);
  text(angle_3, 870, 435);
  text(angle_4, 870, 470);
  text(angle_5, 870, 505);
  text(angle_6, 870, 540);
  
  text("Pot", 950, 345);
  text(pot_1, 950, 365);
  text(pot_2, 950, 400);
  text(pot_3, 950, 435);
  text(pot_4, 950, 470);
  text(pot_5, 950, 505);
  text(pot_6, 950, 540);
  

}

// The following bit of code manages the incoming angles reported by the arduino.
void serialEvent(Serial myPort) {
  if(robot._isPluggedIn) {
   dataReading = myPort.readString();
   if(dataReading!=null){
     
      char inByte = dataReading.charAt(0); 
      println(inByte);
      switch(inByte) 
        {
        case 'a':
          robot.motors()[0].UpdateAngles(float(dataReading.substring(1)));
          angle_1 = dataReading.substring(1);
          break;
        case 'b':
          robot.motors()[1].UpdateAngles(float(dataReading.substring(1)));
                  angle_2 = dataReading.substring(1);
  
          break;
       case 'c':
          robot.motors()[2].UpdateAngles(float(dataReading.substring(1)));
                  angle_3 = dataReading.substring(1);
  
          break;
       case 'd':
          robot.motors()[3].UpdateAngles(float(dataReading.substring(1)));
                  angle_4 = dataReading.substring(1);
  
          break;
       case 'e':
          robot.motors()[4].UpdateAngles(float(dataReading.substring(1)));
                  angle_5 = dataReading.substring(1);
  
          break;
       case 'f':
          robot.motors()[5].UpdateAngles(float(dataReading.substring(1)));
                  angle_6 = dataReading.substring(1);
  
          break;        
        case 'A':
          pot[0] = dataReading.substring(1);
          break;
        case 'B':
          pot[1] = dataReading.substring(1);
          break;
       case 'C':
          pot[2] = dataReading.substring(1);
          break;
       case 'D':
          pot[3] = dataReading.substring(1);
          break;
       case 'E':
          pot[4] = dataReading.substring(1);
          break;   
       case 'F':
          pot[5] = dataReading.substring(1);
          break;  
       case 'Z':
          gpmsg = dataReading.substring(1);
          break;    
        }
      
    }
  } 
}


void sendmotors(){
  
  int writenumber0 = round(robot._motorAngles[0]+0.5);
  int writenumber1 = round(robot._motorAngles[1]+0.5);
  int writenumber2 = round(robot._motorAngles[2]+0.5);
  int writenumber3 = round(robot._motorAngles[3]+0.5);
  int writenumber4 = round(robot._motorAngles[4]+0.5);
  int writenumber5 = round(robot._motorAngles[5]+0.5);

  
    myPort.write("a"+writenumber0);  
    myPort.write("b"+writenumber1);
    myPort.write("c"+writenumber2);
    myPort.write("d"+writenumber3);
    myPort.write("e"+writenumber4);
    myPort.write("f"+writenumber5);


}


// These Functions along with home were migrated to Master Program, they are just kept here as a reference

//// function colorB will receive changes from 
//// controller with name colorB
//public void Relax(int theValue) {
//  println("a button event from colorB: "+theValue);
//  myPort.write("r");
//}


//
//public void GClose(int theValue) {
//  println("a button event from colorC: "+theValue);
//  myPort.write("g");
//}


//public void GOpen(int theValue) {
//  println("a button event from colorD: "+theValue);
//  myPort.write("o");
//}


