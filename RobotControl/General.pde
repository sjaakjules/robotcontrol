
float[][] adjoint(float[][] matrix) {
  int len = matrix[0].length;
  float[][] cofactor = new float[len][len];
  int sign = 1;
  for (int i=0;i<len;i++) {
    for (int j=0;j<len;j++) {
      cofactor[i][j] = sign*subDet(matrix,i,j,len);
      sign *= -1;
    }
  }
  return Mat.transpose(cofactor);
}

float subDet(float[][] matrix, int row, int col, int len) {
  int m, n;
  if (row==0) {m=1;} 
  else        {m=0;}
  if (col==0) {n=1;}
  else        {n=0;}
  float[][] subMatrix = new float[len-1][len-1];
  for (int i=0;i<len;i++) {
    for (int j=0;j<len;j++) {
      if (i!=row && j!=col) {
        subMatrix[m][n] = matrix[i][j];
        n++;
      }
    }
    m++;
  }
  return Mat.det(subMatrix);
}

float[][] zCross(float[][] rowVec) {
  return new float[][]{{-rowVec[1][0]},{rowVec[0][0]},{0}};
}

float[][] Cross(float[][] u, float[][] v) {
  return new float[][]{{u[1][0]*v[2][0]-u[2][0]*v[1][0]},
                       {u[2][0]*v[0][0]-u[0][0]*v[2][0]},
                       {u[0][0]*v[1][0]-u[1][0]*v[0][0]}};
}

float[][] getXYZrotation(float x, float y, float z) {
  float sx,cx,sy,cy,sz,cz;
  sx = sin(radians(x));
  cx = cos(radians(x));
  sy = sin(radians(y));
  cy = cos(radians(y));
  sz = sin(radians(z));
  cz = cos(radians(z));
  float[][] xrot = {{1,  0, 0 },
                    {0, cx,sx},
                    {0, -sx, cx}};
  float[][] yrot = {{cy,  0,-sy },
                    {0 , 1, 0},
                    {sy, 0, cy}};
  float[][] zrot = {{cz,sz, 0},
                    {-sz, cz, 0},
                    {0 ,  0, 1}};
  return Mat.multiply(Mat.multiply(yrot,xrot),zrot);
}

class System {
  // Fields
  private int _last_frame, _frame_time;
  
  // Contructors
  System() {
    _last_frame = millis();
    _frame_time = 1;
  }
  
  // Properties
  int frame_ms() { return _frame_time; }
  float frame_sec() { return (float)_frame_time/1000.0; }
  
  // Methods
  void update() {
    _frame_time = millis()-_last_frame;
    _last_frame = millis();
    if (_frame_time==0) {
      _frame_time = 1;
    }
  }
}


