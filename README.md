RobotControl
============

Processing script to control a 6DOF robot arm

The following is an overview of the source code. 
An indentation represents objects of that class type within the above class.
The object name is in triangle brackets and common variables in curly backets.

TransformMatrix is a class used to discribe the pose of something, rotation and position.
This is used throughout most of the classes and it's common variables shown below.

TransformMatrix
	{float[] pos()}			- Position array [x y z 1]
	{float[][] rotation()}	- 3x3 rotation matrix
	{float[][] posCol()}	- position matrix, 3x1 matrix [x;y;z]
	{float[][] H()}			- 4x4 transform matrix
	{Quaternion Q()}		- quaternion representing the rotation

Kali (main script)
	System <system>
		{int frame_ms()} 	- last draw time in milliseconds
		{float frame_sec()}	- last draw time in seconds
	GUI <gui>
		{PFont display()}	- std font to use in text()
		{PFont title()}		- std title font
		
	UI <ui>
		{Textfield[6] gain} 	- List of Textfields used to control the gain of the controller
		{Slider pX1, pY1, pZ1}	- Sliders used to control the position of the floating tool
		{Slider rX1, rY1, rZ1}	- Sliders used to control the rotation of the floating tool
		{Button go,home			- Buttons used to start moves
				point1,point2}
		
	Robot <robot>
		{TransformMatrix H0t()} 	- 4x4 matrix representing pose of the base to the 7th motor
		{TransformMatrix H0te()}	- 4x4 matrix representing pose of the end effector tip	
		{boolean isMoving()}		- used in command to loop through robotTasks
		{float[6] motorAngles()}	- list of motor angles from base to last wrist	
		{float[3] endPosition()}	- [x y z] position
		{float[3] endVelocity()}	- [x y z] velocity
		{Dh[8] Links()}				- a list of all the links, Links()[6] and up is after the wrist to motor base and tool tip
		
		
		Jacobian <J()>			- robot.J().J() returns float[][]
			{float[][] J()}		- Jacobian
			
		Motor <motors()>		- List of motor objects
			{float angle()}		- returns _lastAngle or sets with floaat to _desiredAngle
			{float velocity()}	- same as angle but with velocity
			
		DH <Links()>				- a list of all the links, Links()[6] and up is after the wrist to motor base and tool tip
			{TransformMatrix H()} 	- returns the 4x4 matrix for that row, 0to1 1to2 2to3 ect
			{boolean isRotating()}	- Bool if that link is rotating, last links dont rotate (wrist to motor base to tool)
			
		Control
			{float[] getControlEffort(poseRef,poseMeasure,velocityRef)}
				This creates the control effort, velocity in taskspace
				
	Model <model>
		This draws the 3D model according to the robot.Links.H().H() (DH objects)
			
	Master_Program <mp>
		Trajectory <movement>
			{float[4] getTipVelocity(time)}		- Returns the velocity [x y z alpha] at a given time.
			{int checkTime(time)}				- returns the index of the current quintic path, 
												  for 3 point trajectory where t0<t1<tf, will return 0 if t0<time<t1 or 1 if t1<time<tf

Planned changes:

--MasterProgram--
	This is the global planning section where button checking and input files are handled to move the robot.
	-- currently it draws a floating gripper from the slidders and controls the buttons to move the robot.
	It will contain new class, atm called commander...
		
Commander <commander>		
		This will be constructed with the list of strings from a txt file, here each line adds entries into robotTasks,
			eg, 	'Move', 'OpenGripper', 'CloseGripper'...
		It will make a list of Trajectory objects of the same length as robotTasks where null pointers occupy non movement tasks such as 'OpenGripper'
		A method will be called with an int, from 0 to length of robotTasks which triggers said task. 		
		A case statement will, over write 'Master_Program.movement' if 'move' or send gripper command to arduino if 'openGripper'
		The loop progression can be controlled with a return true or 1 to Master Program
		
		
	
		
		